import { Branch } from 'src/branch/entities/branch.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { InvoiceDetail } from './invoiceDetail.entity';

@Entity()
export class Invoice {
  @PrimaryGeneratedColumn()
  invoiceId: number;

  @Column()
  type: string;

  @Column()
  total: number;

  @Column({ nullable: true })
  datePay: string;

  @ManyToOne(() => Branch, (branch) => branch.invoices)
  branch: Branch;

  @ManyToOne(() => Employee, (employee) => employee.invoices)
  employee: Employee;

  @OneToMany(() => InvoiceDetail, (invoiceDetail) => invoiceDetail.invoice, {
    onDelete: 'CASCADE',
  })
  invoiceDetails: InvoiceDetail[];
}
