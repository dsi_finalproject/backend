import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Invoice } from './invoice.entity';

@Entity()
export class InvoiceDetail {
  @PrimaryGeneratedColumn()
  invoiceDetailid: number;

  @Column()
  item: string;

  @Column()
  qty: number;

  @Column()
  price: number;

  @Column()
  total: number;

  @Column()
  unit: string;

  @ManyToOne(() => Invoice, (invoice) => invoice.invoiceDetails, {
    onDelete: 'CASCADE',
  })
  invoice: Invoice;
}
