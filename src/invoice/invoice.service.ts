import { Injectable } from '@nestjs/common';
import { CreateInvoiceDto } from './dto/create-invoice.dto';
// import { UpdateInvoiceDto } from './dto/update-invoice.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Invoice } from './entities/invoice.entity';
import { Repository } from 'typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { InvoiceDetail } from './entities/invoiceDetail.entity';
import { UpdateInvoiceDto } from './dto/update-invoice.dto';

@Injectable()
export class InvoiceService {
  constructor(
    @InjectRepository(Invoice)
    private invoiceRepository: Repository<Invoice>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
    @InjectRepository(InvoiceDetail)
    private invoiceDetailRepository: Repository<InvoiceDetail>,
  ) {}
  async create(createInvoiceDto: CreateInvoiceDto) {
    const invoice = new Invoice();
    invoice.type = createInvoiceDto.type;
    invoice.total = createInvoiceDto.total;
    invoice.datePay = createInvoiceDto.datePay;
    const employee = await this.employeeRepository.findOneBy({
      employeeId: createInvoiceDto.employee.employeeId,
    });
    const branch = await this.branchRepository.findOneBy({
      branchId: createInvoiceDto.branch.branchId,
    });
    invoice.employee = employee;
    invoice.branch = branch;
    invoice.invoiceDetails = [];
    for (const invoiceDetail of createInvoiceDto.invoiceDetails) {
      const newIVD = new InvoiceDetail();
      newIVD.item = invoiceDetail.item;
      newIVD.qty = invoiceDetail.qty;
      newIVD.price = invoiceDetail.price;
      newIVD.total = invoiceDetail.total;
      newIVD.unit = invoiceDetail.unit;
      await this.invoiceDetailRepository.save(newIVD);
      invoice.invoiceDetails.push(newIVD);
    }
    return this.invoiceRepository.save(invoice);
  }

  findAll() {
    return this.invoiceRepository.find({
      relations: ['employee', 'branch', 'invoiceDetails'],
    });
  }

  findOne(invoiceId: number) {
    return this.invoiceRepository.findOne({
      where: { invoiceId },
      relations: ['employee', 'branch', 'invoiceDetails'],
    });
  }

  async update(id: number, updateInvoiceDto: UpdateInvoiceDto) {
    return 'NO UPDATE';
  }

  async remove(invoiceId: number) {
    const removeInvoice = await this.invoiceRepository.findOneBy({
      invoiceId: invoiceId,
    });
    return this.invoiceRepository.remove(removeInvoice);
  }
  async removedetail(invoicedetailId: number) {
    const removeInvoicedetail = await this.invoiceDetailRepository.findOneBy({
      invoiceDetailid: invoicedetailId,
    });
    return this.invoiceDetailRepository.remove(removeInvoicedetail);
  }
}
