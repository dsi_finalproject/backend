import { Module } from '@nestjs/common';
import { InvoiceService } from './invoice.service';
import { InvoiceController } from './invoice.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Invoice } from './entities/invoice.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { InvoiceDetail } from './entities/invoiceDetail.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Invoice, Employee, Branch, InvoiceDetail]),
  ],
  controllers: [InvoiceController],
  providers: [InvoiceService],
})
export class InvoiceModule {}
