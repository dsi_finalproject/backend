import { Employee } from 'src/employee/entities/employee.entity';
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Role {
  @PrimaryGeneratedColumn()
  roleId: number;

  @Column()
  name: string;

  @ManyToMany(() => Employee, (employee) => employee.roles)
  employees: Employee[];
}
