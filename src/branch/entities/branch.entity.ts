// import { Receipt } from 'src/receipt/entities/receipt.entity';
// import { BranchStock } from 'src/branchstock/entities/branchstock.entity';
import { BranchStock } from 'src/branchstock/entities/branchstock.entity';
import { CheckStock } from 'src/checkstock/entities/checkstock.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { ImportMaterial } from 'src/import/entities/import.entity';
import { Invoice } from 'src/invoice/entities/invoice.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Salary } from 'src/salary/entities/salary.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  branchId: number;

  @Column()
  name: string;

  @Column()
  address: string;

  @Column()
  phone: string;

  @ManyToMany(() => Employee, (employee) => employee.branch)
  @JoinColumn()
  employees: Employee[];

  @OneToMany(() => Invoice, (invoice) => invoice.branch)
  invoices: Invoice[];

  @OneToMany(() => ImportMaterial, (importMaterial) => importMaterial.branch)
  importMaterial: ImportMaterial[];

  @OneToMany(() => Receipt, (receipt) => receipt.branch)
  receipt: Receipt[];

  @OneToMany(() => BranchStock, (branchStock) => branchStock.branch)
  branchStock: BranchStock[];

  @OneToMany(() => CheckStock, (checkstock) => checkstock.branch)
  checkstock: CheckStock[];

  @OneToMany(() => Salary, (salary) => salary.branch)
  salary: Salary[];

  // @OneToMany(() => Receipt, (receipt) => receipt.branch)
  // receipts: Receipt[];
}
