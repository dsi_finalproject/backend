import { Injectable } from '@nestjs/common';
import { CreateImportDto } from './dto/create-import.dto';
import { UpdateImportDto } from './dto/update-import.dto';
import { ImportMaterial } from './entities/import.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { ImportMaterialDetail } from './entities/importDetail.entity';
import { Material } from 'src/material/entities/material.entity';
import { BranchStock } from 'src/branchstock/entities/branchstock.entity';

@Injectable()
export class ImportMaterialService {
  constructor(
    @InjectRepository(ImportMaterial)
    private ImportRepository: Repository<ImportMaterial>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
    @InjectRepository(ImportMaterialDetail)
    private importMaterialDetailRepository: Repository<ImportMaterialDetail>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
    @InjectRepository(BranchStock)
    private branchStockRepository: Repository<BranchStock>,
  ) {}

  async create(createImportDto: CreateImportDto) {
    const importMaterial = new ImportMaterial();
    importMaterial.date = createImportDto.date;
    importMaterial.total = createImportDto.total;
    importMaterial.totalList = createImportDto.totalList;
    const employee = await this.employeeRepository.findOneBy({
      employeeId: createImportDto.employee.employeeId,
    });
    const branch = await this.branchRepository.findOneBy({
      branchId: createImportDto.branch.branchId,
    });
    importMaterial.employee = employee;
    importMaterial.branch = branch;
    importMaterial.importMaterialDetails = [];
    if (Array.isArray(createImportDto.importMaterialDetails)) {
      for (const newDetail of createImportDto.importMaterialDetails) {
        const newDetailDB = new ImportMaterialDetail();
        newDetailDB.qty = newDetail.qty;
        newDetailDB.unitPrice = newDetail.unitPrice;
        newDetailDB.total = newDetail.total;
        const material = await this.materialRepository.findOneBy({
          materialId: newDetail.material.materialId,
        });
        newDetailDB.material = material;
        if (newDetail.branchstock && newDetail.branchstock.branchStockId) {
          const branchstock = await this.branchStockRepository.findOneBy({
            branchStockId: newDetail.branchstock.branchStockId,
          });
          newDetailDB.branchstock = branchstock;
        } else {
          // Handle the case where newDetail.branchstock is undefined or branchStockId is missing
          console.error('BranchStock or branchStockId is missing');
          // Optionally, you can throw an error, log a message, or handle the situation based on your application's requirements.
        }
        await this.importMaterialDetailRepository.save(newDetailDB);
        importMaterial.importMaterialDetails.push(newDetailDB);
      }
    }

    return this.ImportRepository.save(importMaterial);
  }

  findAll(): Promise<ImportMaterial[]> {
    return this.ImportRepository.find({
      relations: [
        'employee',
        'branch',
        'importMaterialDetails',
        'importMaterialDetails.material',
      ],
    });
  }

  findOne(importMaterialId: number) {
    return this.ImportRepository.findOne({
      where: { importMaterialId },
      relations: [
        'employee',
        'branch',
        'importMaterialDetails',
        'importMaterialDetails.material',
      ],
    });
  }

  async update(id: number, updateImportDto: UpdateImportDto) {
    return 'not use Return';
  }

  async remove(importMaterialId: number) {
    const deleteimportMaterial = await this.ImportRepository.findOneBy({
      importMaterialId,
    });
    return this.ImportRepository.remove(deleteimportMaterial);
  }

  async removeDetail(importMaterialDetailId: number) {
    const importMaterialDetail =
      await this.importMaterialDetailRepository.findOne({
        where: { importMaterialDetailId },
      });
    return this.importMaterialDetailRepository.remove(importMaterialDetail);
  }
}
