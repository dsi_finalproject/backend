import { Module } from '@nestjs/common';
import { ImportMaterialService } from './import.service';
import { ImportController } from './import.controller';
import { ImportMaterial } from './entities/import.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { ImportMaterialDetail } from './entities/importDetail.entity';
import { BranchStock } from 'src/branchstock/entities/branchstock.entity';
import { Material } from 'src/material/entities/material.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ImportMaterial,
      Employee,
      Branch,
      ImportMaterialDetail,
      BranchStock,
      Material,
    ]),
  ],
  controllers: [ImportController],
  providers: [ImportMaterialService],
})
export class importModule {}
