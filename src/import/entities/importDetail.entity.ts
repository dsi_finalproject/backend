import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Material } from 'src/material/entities/material.entity';
import { BranchStock } from 'src/branchstock/entities/branchstock.entity';
import { ImportMaterial } from './import.entity';

@Entity()
export class ImportMaterialDetail {
  @PrimaryGeneratedColumn()
  importMaterialDetailId: number;

  @Column()
  qty: number;

  @Column()
  unitPrice: number;

  @Column()
  total: number;

  @ManyToOne(
    () => ImportMaterial,
    (importMaterial) => importMaterial.importMaterialDetails,
    {
      onDelete: 'CASCADE',
    },
  )
  importMaterial: ImportMaterial;

  @ManyToOne(() => Material, (material) => material.importMaterialDetails)
  material: Material;

  @ManyToOne(
    () => BranchStock,
    (branchstock) => branchstock.importMaterialDetails,
  )
  branchstock: BranchStock;
}
