import { IsNotEmpty } from 'class-validator';

export class CreateImportDto {
  @IsNotEmpty()
  date: string;

  @IsNotEmpty()
  total: number;

  @IsNotEmpty()
  totalList: number;

  @IsNotEmpty()
  employee: { employeeId: number };

  @IsNotEmpty()
  branch: { branchId: number };

  @IsNotEmpty()
  importMaterialDetails: {
    importMaterialDetailId?: number;
    qty: number;
    unitPrice: number;
    total: number;
    material: { materialId: number };
    branchstock: { branchStockId: number };
  }[];
}
