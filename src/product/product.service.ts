import { Injectable, BadRequestException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Productcategory } from 'src/productcategory/entities/productcategory.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    @InjectRepository(Productcategory)
    private productcategoryRepository: Repository<Productcategory>,
  ) {}

  async create(createProductDto: CreateProductDto, image: string) {
    console.log(createProductDto.category);
    const newProduct = new Product();
    newProduct.name = createProductDto.name;
    newProduct.price = createProductDto.price;

    const jsonString = String(createProductDto.category);
    const numberString = jsonString.substring(21, 22);
    const number = parseInt(numberString);

    const category = await this.productcategoryRepository.findOne({
      where: { productcategoryId: number },
    });
    console.log(createProductDto.category);
    newProduct.category = category;
    newProduct.qty = createProductDto.qty;
    newProduct.image = image;
    return this.productRepository.save(newProduct);
  }

  findAll() {
    return this.productRepository.find({ relations: ['category'] });
  }

  findOne(productId: number) {
    return this.productRepository.findOne({
      where: { productId },
      relations: ['category'],
    });
  }

  callPriceDesc() {
    return this.productRepository.query(`SELECT * FROM productCat`);
  }

  async update(id: number, updateProductDto: UpdateProductDto, image: string) {
    console.log(updateProductDto);
    const product = await this.productRepository.findOne({
      where: { productId: id },
    });
    if (!product) {
      throw new BadRequestException('Product not found');
    }
    product.name = updateProductDto.name;
    product.price = updateProductDto.price;
    const jsonString = String(updateProductDto.category);
    const numberString = jsonString.substring(21, 22);
    const number = parseInt(numberString);

    // const productCategoryId = JSON.parse(updateProductDto.category);
    const category = await this.productcategoryRepository.findOne({
      where: { productcategoryId: number },
    });
    product.qty = updateProductDto.qty;
    product.category = category;
    if (image) {
      product.image = image;
    }
    return this.productRepository.save(product);
  }

  async remove(productId: number) {
    const product = await this.productRepository.findOne({
      where: { productId },
    });
    if (!product) {
      throw new BadRequestException('Product not found');
    }
    return this.productRepository.remove(product);
  }
}
