import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  price: number;

  image: string;

  @IsNotEmpty()
  category: { productcategoryId: number };

  @IsOptional()
  qty: number;
}
