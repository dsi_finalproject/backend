// import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  customerId: number;

  @Column()
  name: string;

  @Column()
  phone: string;

  @Column()
  birthDate: string;

  @Column()
  point: number;
  receipt: any;

  // @OneToMany(() => Receipt, (receipt) => receipt.customer)
  // receipt: Receipt[];
}
