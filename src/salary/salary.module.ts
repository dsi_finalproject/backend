import { Module } from '@nestjs/common';
import { SalaryService } from './salary.service';
import { SalaryController } from './salary.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Salary } from './entities/salary.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { Branch } from 'src/branch/entities/branch.entity';
// import { Customer } from 'src/customer/entities/customer.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Salary,
      Employee,
      Checkinout,
      Branch,

      // Customer,
    ]),
  ],
  controllers: [SalaryController],
  providers: [SalaryService],
})
export class SalaryModule {}
