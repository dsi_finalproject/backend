import { Branch } from 'src/branch/entities/branch.entity';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import {
  Column,
  Entity,
  ManyToOne,
  // JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  salaryId: number;

  @Column()
  salaryCreate: string;

  @Column({ nullable: true })
  salaryPay: string;

  @Column()
  salaryTotal: number;

  @Column()
  salaryStatus: string;

  @ManyToOne(() => Branch, (branch) => branch.salary, { nullable: true })
  branch: Branch;

  @ManyToOne(() => Employee, (employee) => employee.salaryObj)
  employee: Employee;

  @OneToMany(() => Checkinout, (checkinout) => checkinout.salarys)
  checkinout: Checkinout[];
}
