import { Injectable } from '@nestjs/common';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Salary } from './entities/salary.entity';
import { Repository } from 'typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { Branch } from 'src/branch/entities/branch.entity';

@Injectable()
export class SalaryService {
  constructor(
    @InjectRepository(Salary)
    private salaryRepository: Repository<Salary>,

    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,

    @InjectRepository(Checkinout)
    private checkinoutRepository: Repository<Checkinout>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
  ) {}
  async create(createSalaryDto: CreateSalaryDto) {
    const salary = new Salary();
    salary.salaryCreate = createSalaryDto.salaryCreate;
    salary.salaryTotal = 0;
    console.log(createSalaryDto.branch.branchId);
    const br = await this.branchRepository.findOne({
      where: { branchId: createSalaryDto.branch.branchId },
    });
    salary.branch = br;
    salary.salaryStatus = createSalaryDto.salaryStatus;
    salary.checkinout = [];
    const emp = await this.employeeRepository.findOne({
      where: { employeeId: createSalaryDto.employee.employeeId },
    });
    salary.employee = emp;
    for (const item of createSalaryDto.checkinout) {
      const chkIO = await this.checkinoutRepository.findOne({
        where: { checkInOutId: item.checkInOutId },
      });
      salary.salaryTotal += chkIO.timeWork * emp.wagePerHour;
      chkIO.used = 1;
      this.checkinoutRepository.save(chkIO);
      salary.checkinout.push(chkIO);
    }
    return await this.salaryRepository.save(salary);
  }

  findAll() {
    return this.salaryRepository.find({
      relations: ['employee', 'checkinout', 'branch'],
    });
  }

  findOne(salaryId: number) {
    return this.salaryRepository.findOne({
      where: { salaryId: salaryId },
      relations: ['employee', 'checkinout', 'branch'],
    });
  }

  async update(salaryId: number, updateSalaryDto: UpdateSalaryDto) {
    const salary = await this.salaryRepository.findOne({
      where: { salaryId: salaryId },
      relations: ['employee'],
    });
    console.log(updateSalaryDto);

    salary.salaryStatus = updateSalaryDto.salaryStatus;
    salary.salaryPay = updateSalaryDto.salaryPay;

    return this.salaryRepository.save(salary);
  }

  async remove(salaryId: number) {
    const removeSalary = await this.salaryRepository.findOne({
      where: { salaryId: salaryId },
      relations: ['checkinout'],
    });
    const backUpSet = [];
    for (const item of removeSalary.checkinout) {
      const itemB = await this.checkinoutRepository.findOne({
        where: { checkInOutId: item.checkInOutId },
        relations: ['employee', 'salarys'],
      });
      const backUp = new Checkinout();
      // backUp = itemB;
      backUp.checkIn = itemB.checkIn;
      backUp.checkOut = itemB.checkOut;
      backUp.employee = itemB.employee;
      backUp.status = itemB.status;
      backUp.timeWork = itemB.timeWork;
      backUp.used = 0;
      // backUpSet.push(backUp);
      this.checkinoutRepository.save(backUp);
    }
    await this.salaryRepository.remove(removeSalary);
    console.log(backUpSet);

    return;
  }
}
