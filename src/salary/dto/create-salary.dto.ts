import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateSalaryDto {
  @IsNotEmpty()
  checkinout: { checkInOutId: number }[];
  @IsNotEmpty()
  employee: { employeeId: number };
  @IsNotEmpty()
  branch: { branchId: number };
  @IsNotEmpty()
  salaryCreate: string;
  @IsOptional()
  salaryPay: string;
  @IsNotEmpty()
  salaryTotal: number;
  @IsNotEmpty()
  salaryStatus: string;
}
