import { Module } from '@nestjs/common';
import { CheckinoutService } from './checkinout.service';
import { CheckinoutController } from './checkinout.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkinout } from './entities/checkinout.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Salary } from 'src/salary/entities/salary.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Checkinout, Employee, Salary])],
  controllers: [CheckinoutController],
  providers: [CheckinoutService],
})
export class CheckinoutModule {}
