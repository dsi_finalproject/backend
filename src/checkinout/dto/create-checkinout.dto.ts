import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateCheckinoutDto {
  @IsNotEmpty()
  checkIn: string;

  @IsOptional()
  checkOut: string;

  @IsOptional()
  timeWork: number;

  @IsOptional()
  status: number;

  @IsOptional()
  used: number;

  @IsNotEmpty()
  employee: { employeeId: number };
}
