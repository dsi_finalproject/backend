import { Employee } from 'src/employee/entities/employee.entity';
import { Salary } from 'src/salary/entities/salary.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Checkinout {
  @PrimaryGeneratedColumn()
  checkInOutId: number;

  @Column()
  checkIn: string;

  @Column({ nullable: true })
  checkOut: string;

  @Column({ type: 'decimal', precision: 10, scale: 2, nullable: true })
  timeWork: number;

  @Column({ nullable: true })
  status: number;

  @Column()
  used: number; //1 used/0 not use

  @ManyToOne(() => Employee, (employee) => employee.checkinout)
  employee: Employee;

  @ManyToOne(() => Salary, (salary) => salary.checkinout, {
    onDelete: 'CASCADE',
  })
  salarys: Salary;
}
