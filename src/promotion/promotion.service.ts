import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Promotion } from './entities/promotion.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
@Injectable()
export class PromotionService {
  constructor(
    @InjectRepository(Promotion)
    private PromotionRepository: Repository<Promotion>,
  ) {}
  create(createPromotionDto: CreatePromotionDto): Promise<Promotion> {
    return this.PromotionRepository.save(createPromotionDto);
  }

  findAll(): Promise<Promotion[]> {
    return this.PromotionRepository.find();
  }

  findOne(id: number) {
    return this.PromotionRepository.findOneBy({ promotionId: id });
  }

  async update(promotionId: number, updatePromotionDto: UpdatePromotionDto) {
    await this.PromotionRepository.update(promotionId, updatePromotionDto);
    const Promotion = await this.PromotionRepository.findOneBy({ promotionId });
    return Promotion;
  }

  async updateStatus(promotionId: number, status: number): Promise<Promotion> {
    const promotion = await this.PromotionRepository.findOneBy({ promotionId });
    if (!promotion) {
      throw new Error('Promotion not found');
    }
    promotion.status = status;
    return this.PromotionRepository.save(promotion);
  }

  async remove(promotionId: number) {
    const deletePromotion = await this.PromotionRepository.findOneBy({
      promotionId,
    });
    return this.PromotionRepository.remove(deletePromotion);
  }
}
