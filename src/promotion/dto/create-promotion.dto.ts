import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreatePromotionDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  status: number;

  @IsOptional()
  discountPercentage?: number;

  @IsOptional()
  discountBaht?: number;

  @IsOptional()
  usePoint?: number;

  @IsOptional()
  limitMenu?: number;
}
