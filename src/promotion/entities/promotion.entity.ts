import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  promotionId: number;

  @Column()
  name: string;

  @Column()
  status: number;

  @Column({ nullable: true })
  discountPercentage: number;

  @Column({ nullable: true })
  discountBaht: number;

  @Column({ nullable: true })
  usePoint: number;

  @Column({ nullable: true })
  limitMenu: number;

  @OneToMany(() => Receipt, (receipt) => receipt.promotion)
  receipt: Receipt[];
}
