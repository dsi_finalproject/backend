import { Module } from '@nestjs/common';
import { CheckStockService } from './checkstock.service';
import { CheckStockController } from './checkstock.controller';
import { CheckStock } from './entities/checkstock.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { CheckStockDetail } from './entities/checkstockDetail.entity';
import { BranchStock } from 'src/branchstock/entities/branchstock.entity';
import { Material } from 'src/material/entities/material.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      CheckStock,
      Employee,
      Branch,
      CheckStockDetail,
      BranchStock,
      Material,
    ]),
  ],
  controllers: [CheckStockController],
  providers: [CheckStockService],
})
export class CheckStockModule {}
