import { Branch } from 'src/branch/entities/branch.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { OneToMany } from 'typeorm';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CheckStockDetail } from './checkstockDetail.entity';
@Entity()
export class CheckStock {
  @PrimaryGeneratedColumn()
  checkStockId: number;

  @Column()
  date: string;

  @ManyToOne(() => Employee, (employee) => employee.checkstock)
  employee: Employee;

  @ManyToOne(() => Branch, (branch) => branch.checkstock)
  branch: Branch;

  @OneToMany(
    () => CheckStockDetail,
    (checkstockDetail) => checkstockDetail.checkstock,
  )
  checkstockDetails: CheckStockDetail[];
}
