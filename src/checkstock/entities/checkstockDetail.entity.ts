import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CheckStock } from './checkstock.entity';
import { Material } from 'src/material/entities/material.entity';
import { BranchStock } from 'src/branchstock/entities/branchstock.entity';

@Entity()
export class CheckStockDetail {
  @PrimaryGeneratedColumn()
  checkstockDetailId: number;

  @Column()
  checkLast: number;

  @Column()
  checkRemain: number;

  @Column()
  used: number;

  @ManyToOne(() => CheckStock, (checkstock) => checkstock.checkstockDetails, {
    onDelete: 'CASCADE',
  })
  checkstock: CheckStock;

  @ManyToOne(() => Material, (material) => material.checkstockDetails)
  material: Material;

  @ManyToOne(() => BranchStock, (branchstock) => branchstock.checkstockDetails)
  branchstock: BranchStock;
}
