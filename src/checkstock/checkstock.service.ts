import { Injectable } from '@nestjs/common';
import { CreateCheckStockDto } from './dto/create-checkstock.dto';
import { UpdateCheckStockDto } from './dto/update-checkstock.dto';
import { CheckStock } from './entities/checkstock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { CheckStockDetail } from './entities/checkstockDetail.entity';
import { Material } from 'src/material/entities/material.entity';
import { BranchStock } from 'src/branchstock/entities/branchstock.entity';

@Injectable()
export class CheckStockService {
  constructor(
    @InjectRepository(CheckStock)
    private checkStockRepository: Repository<CheckStock>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
    @InjectRepository(CheckStockDetail)
    private checkStockDetailRepository: Repository<CheckStockDetail>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
    @InjectRepository(BranchStock)
    private branchStockRepository: Repository<BranchStock>,
  ) {}

  async create(createCheckStockDto: CreateCheckStockDto) {
    const checkStock = new CheckStock();
    checkStock.date = createCheckStockDto.date;
    const employee = await this.employeeRepository.findOneBy({
      employeeId: createCheckStockDto.employee.employeeId,
    });
    const branch = await this.branchRepository.findOneBy({
      branchId: createCheckStockDto.branch.branchId,
    });
    checkStock.employee = employee;
    checkStock.branch = branch;
    checkStock.checkstockDetails = [];
    if (Array.isArray(createCheckStockDto.checkstockDetails)) {
      for (const newDetail of createCheckStockDto.checkstockDetails) {
        const newDetailDB = new CheckStockDetail();
        newDetailDB.checkLast = newDetail.checkLast;
        newDetailDB.checkRemain = newDetail.checkRemain;
        newDetailDB.used = newDetail.used;
        const material = await this.materialRepository.findOneBy({
          materialId: newDetail.material.materialId,
        });
        newDetailDB.material = material;
        if (newDetail.branchstock && newDetail.branchstock.branchStockId) {
          const branchstock = await this.branchStockRepository.findOneBy({
            branchStockId: newDetail.branchstock.branchStockId,
          });
          newDetailDB.branchstock = branchstock;
        } else {
        }
        await this.checkStockDetailRepository.save(newDetailDB);
        checkStock.checkstockDetails.push(newDetailDB);
      }
    }
    return this.checkStockRepository.save(checkStock);
  }

  findAll(): Promise<CheckStock[]> {
    return this.checkStockRepository.find({
      relations: [
        'employee',
        'branch',
        'checkstockDetails',
        'checkstockDetails.material',
      ],
    });
  }

  findOne(checkStockId: number) {
    return this.checkStockRepository.findOne({
      where: { checkStockId },
      relations: [
        'employee',
        'branch',
        'checkstockDetails',
        'checkstockDetails.material',
      ],
    });
  }

  async update(id: number, updateCheckStockDto: UpdateCheckStockDto) {
    return 'not use Return';
  }

  async remove(checkStockId: number) {
    const deletecheckStock = await this.checkStockRepository.findOneBy({
      checkStockId,
    });
    return this.checkStockRepository.remove(deletecheckStock);
  }
}
