import { IsNotEmpty } from 'class-validator';

export class CreateCheckStockDto {
  @IsNotEmpty()
  date: string;

  @IsNotEmpty()
  employee: { employeeId: number };

  @IsNotEmpty()
  branch: { branchId: number };

  @IsNotEmpty()
  checkstockDetails: {
    checkstockDetailId?: number;
    checkLast: number;
    checkRemain: number;
    used: number;
    material: { materialId: number };
    branchstock: { branchStockId: number };
  }[];
}
