import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { EmployeeService } from './employee.service';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Employee } from './entities/employee.entity';

@Controller('employee')
export class EmployeeController {
  constructor(
    private readonly employeeService: EmployeeService,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}

  @Post()
  create(@Body() createEmployeeDto: CreateEmployeeDto) {
    console.log(createEmployeeDto.name);
    return this.employeeService.create(createEmployeeDto);
  }

  @Get()
  findAll() {
    return this.employeeService.findAll();
  }

  @Get('sp/:id/date/:date')
  findOneSpDate(@Param('id') id: string, @Param('date') date: string) {
    return this.employeeService.findOneSpDate(id, date);
  }

  @Get('sp/:id')
  findOneSp(@Param('id') id: string) {
    return this.employeeRepository.query(
      'CALL CheckEmployeePerformance(' + id + ')',
    );
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.employeeService.findOne(+id);
  }

  // @Get(':')
  // findOne(@Param('id') id: string) {
  //   return this.employeeService.findOne(+id);
  // }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateEmployeeDto: UpdateEmployeeDto,
  ) {
    return this.employeeService.update(+id, updateEmployeeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.employeeService.remove(+id);
  }
}
