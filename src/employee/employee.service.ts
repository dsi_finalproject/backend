import { Injectable } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Branch } from 'src/branch/entities/branch.entity';
import { Repository } from 'typeorm';
import { Role } from 'src/roles/entities/role.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
    @InjectRepository(Role)
    private roleRepository: Repository<Role>,
  ) {}
  async create(createEmployeeDto: CreateEmployeeDto) {
    console.log('Ma');
    console.log(createEmployeeDto);
    const newEmp = new Employee();
    newEmp.name = createEmployeeDto.name;
    newEmp.email = createEmployeeDto.email;
    const saltOrRounds = 10;
    newEmp.password = await bcrypt.hash(
      createEmployeeDto.password,
      saltOrRounds,
    );
    newEmp.gender = createEmployeeDto.gender;
    newEmp.salaryType = createEmployeeDto.salaryType;
    newEmp.salary = createEmployeeDto.salary ?? 0;
    if (createEmployeeDto.salaryType === 'Full-Time') {
      newEmp.wagePerHour = createEmployeeDto.salary / 30 / 12;
    } else {
      newEmp.wagePerHour = createEmployeeDto.wagePerHour;
    }

    newEmp.phone = createEmployeeDto.phone;

    newEmp.branch = [];
    newEmp.roles = [];

    for (const itemRole of createEmployeeDto.roles) {
      const role = await this.roleRepository.findOne({
        where: { roleId: itemRole.roleId },
      });
      newEmp.roles.push(role);
    }

    for (const itemBranch of createEmployeeDto.branch) {
      const branch = await this.branchRepository.findOne({
        where: { branchId: itemBranch.branchId },
      });

      newEmp.branch.push(branch);
    }
    return this.employeeRepository.save(newEmp);
  }

  findAll() {
    return this.employeeRepository.find({ relations: ['branch', 'roles'] });
  }

  findOneSpDate(id: string, date: string) {
    return this.employeeRepository.query(`CALL EvaluateQty('${id}','${date}')`);
  }

  findOne(employeeId: number) {
    return this.employeeRepository.findOne({
      where: { employeeId },
      relations: ['branch', 'roles'],
    });
  }

  async update(employeeId: number, updateEmployeeDto: UpdateEmployeeDto) {
    const emp = await this.employeeRepository.findOne({
      where: { employeeId: employeeId },
    });
    if (updateEmployeeDto.name !== undefined) {
      emp.name = updateEmployeeDto.name;
    }
    if (updateEmployeeDto.email !== undefined) {
      emp.email = updateEmployeeDto.email;
    }
    if (updateEmployeeDto.password !== '') {
      const saltOrRounds = 10;
      emp.password = await bcrypt.hash(
        updateEmployeeDto.password,
        saltOrRounds,
      );
    }
    if (updateEmployeeDto.gender !== undefined) {
      emp.gender = updateEmployeeDto.gender;
    }
    if (updateEmployeeDto.salaryType !== undefined) {
      emp.salaryType = updateEmployeeDto.salaryType;
    }
    if (updateEmployeeDto.salary !== undefined) {
      emp.salary = updateEmployeeDto.salary;
      if (emp.salaryType === 'Full-Time') {
        emp.wagePerHour = updateEmployeeDto.salary / 30 / 12;
      } else {
        if (updateEmployeeDto.wagePerHour !== undefined) {
          emp.wagePerHour = updateEmployeeDto.wagePerHour;
        }
      }
    }
    if (updateEmployeeDto.phone !== undefined) {
      emp.phone = updateEmployeeDto.phone;
    }

    if (updateEmployeeDto.roles !== undefined) {
      emp.roles = [];
      for (const item of updateEmployeeDto.roles) {
        const r = await this.roleRepository.findOne({
          where: { roleId: item.roleId },
        });
        emp.roles.push(r);
      }
    }
    if (updateEmployeeDto.branch !== undefined) {
      emp.branch = [];
      for (const item of updateEmployeeDto.branch) {
        const b = await this.branchRepository.findOne({
          where: { branchId: item.branchId },
        });
        emp.branch.push(b);
      }
    }
    return this.employeeRepository.save(emp);
  }

  async remove(employeeId: number) {
    const removeEmployee = await this.employeeRepository.findOne({
      where: { employeeId },
    });
    return this.employeeRepository.remove(removeEmployee);
  }

  async findOneForLogin(email: string): Promise<Employee | undefined> {
    return this.employeeRepository.findOne({
      where: { email: email },
      relations: ['branch', 'roles'],
    });
  }
}
