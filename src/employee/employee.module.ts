import { Module } from '@nestjs/common';
import { EmployeeService } from './employee.service';
import { EmployeeController } from './employee.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from './entities/employee.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { Role } from 'src/roles/entities/role.entity';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { Invoice } from 'src/invoice/entities/invoice.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { CheckStock } from 'src/checkstock/entities/checkstock.entity';
import { Salary } from 'src/salary/entities/salary.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Employee,
      Branch,
      Role,
      Checkinout,
      Invoice,
      Receipt,
      CheckStock,
      Salary,
    ]),
  ],
  controllers: [EmployeeController],
  providers: [EmployeeService],
  exports: [EmployeeService],
})
export class EmployeeModule {}
