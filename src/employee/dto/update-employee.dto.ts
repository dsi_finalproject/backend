import { PartialType } from '@nestjs/swagger';
import { CreateEmployeeDto } from './create-employee.dto';

export class UpdateEmployeeDto extends PartialType(CreateEmployeeDto) {
  employeeId: number;
  name: string;
  email: string;
  password: string;
  gender: string;
  salary: number;
  phone: string;
  wagePerHour: number;
  roles: import('../../roles/entities/role.entity').Role[];
  branch: import('../../branch/entities/branch.entity').Branch[];
}
