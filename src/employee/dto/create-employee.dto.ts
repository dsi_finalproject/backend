import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateEmployeeDto {
  @IsOptional()
  employeeId: number;
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  email: string;
  @IsOptional()
  password: string;
  @IsNotEmpty()
  roles: { roleId: number }[];
  @IsNotEmpty()
  gender: string;
  @IsNotEmpty()
  salaryType: string;
  @IsOptional()
  salary: number;
  @IsNotEmpty()
  phone: string;
  @IsNotEmpty()
  wagePerHour: number;
  @IsNotEmpty()
  branch: { branchId: number }[];
  @IsOptional()
  salaryCreate: string;
  @IsOptional()
  salaryPay: string;
  @IsOptional()
  salaryTotal: number;
  @IsOptional()
  salaryStatus: string;
}
