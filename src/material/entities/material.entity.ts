// import { Branch } from 'src/branch/entities/branch.entity';
import { BranchStock } from 'src/branchstock/entities/branchstock.entity';
import { CheckStockDetail } from 'src/checkstock/entities/checkstockDetail.entity';
import { ImportMaterialDetail } from 'src/import/entities/importDetail.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
// import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  materialId: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  minimum: number;

  @Column()
  unit: string;

  @OneToMany(() => BranchStock, (branchstock) => branchstock.material)
  branchStock: BranchStock[];

  @OneToMany(
    () => CheckStockDetail,
    (checkstockDetail) => checkstockDetail.material,
  )
  checkstockDetails: CheckStockDetail[];

  @OneToMany(
    () => ImportMaterialDetail,
    (importDetail) => importDetail.material,
  )
  importMaterialDetails: ImportMaterialDetail[];

  //   @OneToMany(() => Branch, (branch) => branch.material)
  //   branch: Branch[];
}
