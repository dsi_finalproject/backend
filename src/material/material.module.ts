import { Module } from '@nestjs/common';
import { MaterialService } from './material.service';
import { MaterialController } from './material.controller';
import { Material } from './entities/material.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckStockDetail } from 'src/checkstock/entities/checkstockDetail.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Material, CheckStockDetail])],
  controllers: [MaterialController],
  providers: [MaterialService],
})
export class MaterialModule {}
