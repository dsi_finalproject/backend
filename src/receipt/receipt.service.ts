import { Injectable } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Repository } from 'typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { ReceiptDetail } from './entities/receiptDetail.entity';
import { Branch } from 'src/branch/entities/branch.entity';
// import { Customer } from 'src/customer/entities/customer.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';
import { Product } from 'src/product/entities/product.entity';
import { Customer } from 'src/customer/entities/customer.entity';

@Injectable()
export class ReceiptService {
  constructor(
    @InjectRepository(Receipt)
    private receiptRepository: Repository<Receipt>,
    @InjectRepository(ReceiptDetail)
    private receiptDetailRepository: Repository<ReceiptDetail>, // เพิ่ม ReceiptDetailRepository ที่นี่
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
    @InjectRepository(Promotion)
    private promotionRepository: Repository<Promotion>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) {}
  async create(createReceiptDto: CreateReceiptDto) {
    const receipt = new Receipt();
    receipt.queue = createReceiptDto.queue;
    receipt.receiptDate = createReceiptDto.receiptDate;
    receipt.totalBefore = createReceiptDto.totalBefore;
    receipt.discount = createReceiptDto.discount;
    receipt.total = createReceiptDto.total;
    receipt.receiveAmount = createReceiptDto.receiveAmount;
    receipt.change = createReceiptDto.change;
    receipt.payment = createReceiptDto.payment;
    receipt.getPoint = createReceiptDto.getPoint;
    receipt.customerPointBefore = createReceiptDto.customerPointBefore ?? 0;
    receipt.birthDay = createReceiptDto.birthDay;

    const employee = await this.employeeRepository.findOneBy({
      employeeId: createReceiptDto.employee.employeeId,
    });
    receipt.employee = employee;

    if (createReceiptDto.promotion) {
      // Check if promotion object is defined
      const promotion = await this.promotionRepository.findOneBy({
        promotionId: createReceiptDto.promotion.promotionId,
      });
      receipt.promotion = promotion;
    }

    const branch = await this.branchRepository.findOneBy({
      branchId: createReceiptDto.branch.branchId,
    });
    receipt.branch = branch;

    const customer = await this.customerRepository.findOneBy({
      customerId: createReceiptDto.customer.customerId,
    });
    receipt.customer = customer;

    receipt.receiptDetail = [];
    if (Array.isArray(createReceiptDto.receiptDetail)) {
      for (const rcDetail of createReceiptDto.receiptDetail) {
        const rcDetailDB = new ReceiptDetail();
        rcDetailDB.qty = rcDetail.qty;
        rcDetailDB.price = rcDetail.price;
        rcDetailDB.total = rcDetail.total;
        rcDetailDB.productCategory = rcDetail.productCategory;
        rcDetailDB.productName = rcDetail.productName;

        if (rcDetail.productSubCategory.toString() === 'เย็น') {
          rcDetailDB.productSubCategory = 1;
        } else if (rcDetail.productSubCategory.toString() === 'ร้อน') {
          rcDetailDB.productSubCategory = 2;
        } else if (rcDetail.productSubCategory.toString() === 'ปั่น') {
          rcDetailDB.productSubCategory = 3;
        }

        switch (rcDetail.productSweetLevel.toString()) {
          case '0 %':
            rcDetailDB.productSweetLevel = 1;
            break;
          case '25 %':
            rcDetailDB.productSweetLevel = 2;
            break;
          case '50 %':
            rcDetailDB.productSweetLevel = 3;
            break;
          case '75 %':
            rcDetailDB.productSweetLevel = 4;
            break;
          case '100 %':
            rcDetailDB.productSweetLevel = 5;
            break;
          default:
            // กรณีไม่ตรงกับเงื่อนไขใดๆ
            break;
        }
        const product = await this.productRepository.findOneBy({
          productId: rcDetail.product.productId,
        });
        rcDetailDB.product = product;
        await this.receiptDetailRepository.save(rcDetailDB);
        receipt.receiptDetail.push(rcDetailDB);
      }
    }

    return this.receiptRepository.save(receipt);
  }

  findAll() {
    return this.receiptRepository.find({
      relations: [
        'employee',
        'promotion',
        'branch',
        'customer',
        'receiptDetail',
      ],
    });
  }

  findOne(receiptId: number) {
    return this.receiptRepository.findOne({
      where: { receiptId: receiptId },
      relations: [
        'employee',
        'promotion',
        'branch',
        'customer',
        'receiptDetail',
      ],
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  update(id: number, updateReceiptDto: UpdateReceiptDto) {
    return `This action updates a #${id} receipt`;
  }

  async remove(receiptId: number) {
    const removeReceipt = await this.receiptRepository.findOne({
      where: { receiptId: receiptId },
    });
    return this.receiptRepository.remove(removeReceipt);
  }

  // reportBestSeller(){
  //   return this.dataSource.query(
  //     `SELECT * FROM receipt_detail`
  //   )
  // }

  // reportBestSeller(){
  //   return this.dataSource.query(
  //     `SELECT  productName AS product_name,
  //     SUM(qty) AS total_quantity
  //     FROM receipt_detail
  //     GROUP BY  product_name
  //     ORDER BY total_quantity DESC
  //     LIMIT 10;`
  //   )
  // }

  // async reportBestSeller() {
  //   return this.receiptDetailRepository
  //     .createQueryBuilder('receipt_detail')
  //     .select(['receipt_detail.productName AS product_name', 'SUM(receipt_detail.qty) AS total_quantity'])
  //     .groupBy('receipt_detail.productName')
  //     .orderBy('total_quantity', 'DESC')
  //     .limit(10)
  //     .getRawMany();
  // }
}
