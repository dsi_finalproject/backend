import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Receipt } from './receipt.entity';
import { Product } from 'src/product/entities/product.entity';

@Entity()
export class ReceiptDetail {
  @PrimaryGeneratedColumn()
  receiptDetailId: number;

  @Column()
  qty: number;

  @Column()
  price: number;

  @Column()
  total: number;

  @Column({})
  productName: string;

  @Column()
  productCategory: string;

  @Column({ nullable: true })
  productSubCategory: number;

  @Column({ nullable: true })
  productSweetLevel: number;

  @ManyToOne(() => Receipt, (receipt) => receipt.receiptDetail, {
    onDelete: 'CASCADE',
  })
  receipt: Receipt;

  @ManyToOne(() => Product, (product) => product.receiptDetails)
  product: Product;
}
