import { Branch } from 'src/branch/entities/branch.entity';
import { Customer } from 'src/customer/entities/customer.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ReceiptDetail } from './receiptDetail.entity';

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  receiptId: number;

  @Column({ default: false })
  birthDay: boolean;

  @Column()
  queue: number;

  @Column()
  receiptDate: string;

  @Column({ type: 'decimal', precision: 10, scale: 2 })
  totalBefore: number;

  @Column({ type: 'decimal', precision: 10, scale: 2 })
  discount: number;

  @Column({ type: 'decimal', precision: 10, scale: 2 })
  total: number;

  @Column({ type: 'decimal', precision: 10, scale: 2 })
  receiveAmount: number;

  @Column({ type: 'decimal', precision: 10, scale: 2 })
  change: number;

  @Column()
  payment: string;

  @Column()
  getPoint: number;

  @Column()
  customerPointBefore?: number;

  @ManyToOne(() => Employee, (employee) => employee.receipt, {
    onDelete: 'SET NULL',
  })
  employee: Employee;

  @ManyToOne(() => Branch, (branch) => branch.receipt, {
    onDelete: 'SET NULL',
  })
  branch: Branch;

  @ManyToOne(() => Promotion, (promotion) => promotion.receipt, {
    onDelete: 'SET NULL',
  })
  promotion: Promotion;

  @ManyToOne(() => Customer, (customer) => customer.receipt, {
    onDelete: 'SET NULL',
  })
  customer: Customer;

  @OneToMany(() => ReceiptDetail, (receiptDetail) => receiptDetail.receipt)
  receiptDetail: ReceiptDetail[];
}
