import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';

@Injectable()
export class ReportService {
  constructor(private dataSource: DataSource) { }

  reportBestSeller() {
    return this.dataSource.query(
      `SELECT  productName AS product_name,
      SUM(qty) AS total_quantity
      FROM receipt_detail
      GROUP BY  product_name
      ORDER BY total_quantity DESC
      LIMIT 10;`,
    );
  }

  reportMaterial() {
    return this.dataSource.query(`
      SELECT 
    material.materialId,
    material.name AS name,
    SUM(import_material_detail.qty) AS totalUsedQuantity
    FROM 
    import_material_detail
    JOIN 
    import_material ON import_material_detail.importMaterialImportMaterialId = import_material.importMaterialId
    JOIN 
    material ON import_material_detail.materialMaterialId = material.materialId
    GROUP BY 
    material.materialId, name;`,
    );
  }
  reportSaleDaily() {
    return this.dataSource.query(`
    SELECT DATE(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s')) AS receiptDate, SUM(total) AS total_daily_sales
    FROM receipt
    GROUP BY DATE(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s'))
    LIMIT 10;`,
    );
  }
  reportSaleMonthly() {
    return this.dataSource.query(`
  SELECT DATE_FORMAT(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s'), '%Y-%m') AS month_year, SUM(total) AS total_monthly_sales
FROM receipt
GROUP BY DATE_FORMAT(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s'), '%Y-%m')
LIMIT 10;`,
    );
  }
  reportSaleYearly() {
    return this.dataSource.query(`
  SELECT YEAR(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s')) AS year, SUM(total) AS total_yearly_sales
FROM receipt
GROUP BY YEAR(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s'))
LIMIT 10;`,
    );
  }
  reportSaleDailyEachEmp() {
    return this.dataSource.query(`
  SELECT employeeEmployeeId, DATE(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s')) AS receiptDate, SUM(total) AS total_daily_sales
FROM receipt
GROUP BY employeeEmployeeId, DATE(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s'))
LIMIT 10;`,
    );
  }
  reportSaleMonthlyEachEmp() {
    return this.dataSource.query(`
    SELECT employeeEmployeeId, DATE_FORMAT(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s'), '%Y-%m') AS month_year, SUM(total) AS total_monthly_sales
    FROM receipt
    GROUP BY employeeEmployeeId, DATE_FORMAT(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s'), '%Y-%m')
    LIMIT 10;
    `,
    );
  }
  reportSaleYearlyEachEmp() {
    return this.dataSource.query(`
    SELECT employeeEmployeeId, YEAR(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s')) AS year, SUM(total) AS total_yearly_sales
    FROM receipt
    GROUP BY employeeEmployeeId, YEAR(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s'))
    LIMIT 10;
    `,
    );
  }
  reportSaleDailyEachBranch() {
    return this.dataSource.query(`
  SELECT employeeEmployeeId, DATE(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s')) AS receiptDate, SUM(total) AS total_daily_sales
FROM receipt
GROUP BY employeeEmployeeId, DATE(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s'))
LIMIT 10;`,
    );
  }
  reportSaleMonthlyEachBranch() {
    return this.dataSource.query(`
    SELECT employeeEmployeeId, DATE_FORMAT(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s'), '%Y-%m') AS month_year, SUM(total) AS total_monthly_sales
    FROM receipt
    GROUP BY employeeEmployeeId, DATE_FORMAT(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s'), '%Y-%m')
    LIMIT 10;
    `,
    );
  }
  reportSaleYearlyEachBranch() {
    return this.dataSource.query(`
    SELECT employeeEmployeeId, YEAR(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s')) AS year, SUM(total) AS total_yearly_sales
FROM receipt
GROUP BY employeeEmployeeId, YEAR(STR_TO_DATE(receiptDate, '%d/%m/%Y %H:%i:%s'))
LIMIT 10;

    `,
    );
  }
}
