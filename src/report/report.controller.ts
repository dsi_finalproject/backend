import { Controller, Get } from '@nestjs/common';
import { ReportService } from './report.service';

@Controller('report')
export class ReportController {
  constructor(private reportService: ReportService) { }
  @Get('/reportBestSeller')
  reportBestSeller() {
    return this.reportService.reportBestSeller();
  }

  @Get('/reportMaterial')
  reportMaterial() {
    return this.reportService.reportMaterial();
  }

  @Get('/reportSaleDaily')
  reportSaleDaily() {
    return this.reportService.reportSaleDaily();
  }
  @Get('/reportSaleMonthly')
  reportSaleMonthly() {
    return this.reportService.reportSaleMonthly();
  }
  @Get('/reportSaleYearly')
  reportSaleYearly() {
    return this.reportService.reportSaleYearly();
  }

  @Get('/reportSaleDailyEachEmp')
  reportSaleDailyEachEmp() {
    return this.reportService.reportSaleDailyEachEmp();
  }
  @Get('/reportSaleMonthlyEachEmp')
  reportSaleMonthlyEachEmp() {
    return this.reportService.reportSaleMonthlyEachEmp();
  }
  @Get('/reportSaleYearlyEachEmp')
  reportSaleYearlyEachEmp() {
    return this.reportService.reportSaleYearlyEachEmp();
  }

  @Get('/reportSaleDailyEachBranch')
  reportSaleDailyEachBranch() {
    return this.reportService.reportSaleDailyEachBranch();
  }
  @Get('/reportSaleMonthlyEachBranch')
  reportSaleMonthlyEachBranch() {
    return this.reportService.reportSaleMonthlyEachBranch();
  }
  @Get('/reportSaleYearlyEachBranch')
  reportSaleYearlyEachBranch() {
    return this.reportService.reportSaleYearlyEachBranch();
  }
}
