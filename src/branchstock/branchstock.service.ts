import { Injectable } from '@nestjs/common';
import { CreateBranchStockDto } from './dto/create-branchstock.dto';
import { UpdateBranchStockDto } from './dto/update-branchstock.dto';
import { BranchStock } from './entities/branchstock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Material } from 'src/material/entities/material.entity';
import { Branch } from 'src/branch/entities/branch.entity';

@Injectable()
export class BranchStockService {
  constructor(
    @InjectRepository(BranchStock)
    private branchStockRepository: Repository<BranchStock>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
  ) {}

  async create(createBranchStockDto: CreateBranchStockDto) {
    const branchStock = new BranchStock();
    branchStock.qty = 0;
    const material = await this.materialRepository.findOneBy({
      materialId: createBranchStockDto.material.materialId,
    });
    const branch = await this.branchRepository.findOneBy({
      branchId: createBranchStockDto.branch.branchId,
    });
    branchStock.material = material;
    branchStock.branch = branch;

    return this.branchStockRepository.save(branchStock);
  }

  findAll(): Promise<BranchStock[]> {
    return this.branchStockRepository.find({
      relations: ['branch', 'material'],
    });
  }

  findOne(branchStockId: number) {
    return this.branchStockRepository.findOne({
      where: { branchStockId },
      relations: ['branch', 'material'],
    });
  }

  async update(branchStockId: number, updateCheckStockDto: UpdateBranchStockDto) {
    await this.branchStockRepository.update(branchStockId, updateCheckStockDto);
    const BranchStock = await this.branchStockRepository.findOneBy({ branchStockId });
    return BranchStock;
  }

  async remove(branchStockId: number) {
    const deletebranchStock = await this.branchStockRepository.findOneBy({
      branchStockId,
    });
    return this.branchStockRepository.remove(deletebranchStock);
  }
}
