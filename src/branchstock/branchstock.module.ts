import { Module } from '@nestjs/common';
import { BranchStockService } from './branchstock.service';
import { BranchStockController } from './branchstock.controller';
import { BranchStock } from './entities/branchstock.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
// import { Material } from 'src/material/entities/material.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { Material } from 'src/material/entities/material.entity';
import { CheckStockDetail } from 'src/checkstock/entities/checkstockDetail.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([BranchStock, Branch, Material, CheckStockDetail]),
  ],
  controllers: [BranchStockController],
  providers: [BranchStockService],
})
export class BranchStockModule {}
