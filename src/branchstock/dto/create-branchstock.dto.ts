import { IsNotEmpty } from 'class-validator';

export class CreateBranchStockDto {
  @IsNotEmpty()
  branch: { branchId: number };

  @IsNotEmpty()
  material: { materialId: number };
}
