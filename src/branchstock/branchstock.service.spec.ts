import { Test, TestingModule } from '@nestjs/testing';
import { BranchStockService } from './branchstock.service';

describe('BranchStockService', () => {
  let service: BranchStockService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BranchStockService],
    }).compile();

    service = module.get<BranchStockService>(BranchStockService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
