import { Branch } from 'src/branch/entities/branch.entity';
import { CheckStockDetail } from 'src/checkstock/entities/checkstockDetail.entity';
import { ImportMaterialDetail } from 'src/import/entities/importDetail.entity';
import { Material } from 'src/material/entities/material.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
@Entity()
export class BranchStock {
  @PrimaryGeneratedColumn()
  branchStockId: number;

  @Column()
  qty: number;

  @ManyToOne(() => Branch, (branch) => branch.branchStock)
  branch: Branch;

  @ManyToOne(() => Material, (material) => material.branchStock)
  material: Material;

  @OneToMany(
    () => CheckStockDetail,
    (checkstockDetail) => checkstockDetail.branchstock,
  )
  checkstockDetails: CheckStockDetail[];

  @OneToMany(
    () => ImportMaterialDetail,
    (importDetail) => importDetail.importMaterial,
  )
  importMaterialDetails: ImportMaterialDetail[];
}
