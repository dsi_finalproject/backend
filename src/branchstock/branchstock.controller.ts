import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { BranchStockService } from './branchstock.service';
import { CreateBranchStockDto } from './dto/create-branchstock.dto';
import { UpdateBranchStockDto } from './dto/update-branchstock.dto';

@Controller('branchstock')
export class BranchStockController {
  constructor(private readonly branchStockService: BranchStockService) {}

  @Post()
  create(@Body() createBranchStockDto: CreateBranchStockDto) {
    return this.branchStockService.create(createBranchStockDto);
  }

  @Get()
  findAll() {
    return this.branchStockService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.branchStockService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBranchStockDto: UpdateBranchStockDto,
  ) {
    return this.branchStockService.update(+id, updateBranchStockDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.branchStockService.remove(+id);
  }
}
